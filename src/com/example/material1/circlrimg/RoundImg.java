package com.example.material1.circlrimg;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.Bitmap.Config;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.ImageView;

public class RoundImg extends ImageView {

	public RoundImg(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
	}
	protected void onDraw(Canvas canvas) {

	    Drawable drawable = getDrawable();

	    if (drawable == null) {
	        return;
	    }

	    if (getWidth() == 0 || getHeight() == 0) {
	        return; 
	    }
	    Bitmap b =  ((BitmapDrawable)drawable).getBitmap() ;
	    Bitmap bitmap = b.copy(Bitmap.Config.ARGB_8888, true);

	    int w = getWidth(), h = getHeight();


	    Bitmap roundBitmap =  getRoundedCroppedBitmap(bitmap, w);
	    canvas.drawBitmap(roundBitmap, 0,0, null);

	}
	public static Bitmap getRoundedCroppedBitmap(Bitmap bitmap, int radius) {
	    Bitmap finalBitmap=bitmap;
	    if(bitmap.getWidth() != radius || bitmap.getHeight() != radius)
	        finalBitmap = Bitmap.createScaledBitmap(bitmap, radius, radius, false);
	    else
	        finalBitmap = bitmap;
	    Bitmap output = Bitmap.createBitmap(finalBitmap.getWidth(),
	            finalBitmap.getHeight(), Config.ARGB_8888);
	    Canvas canvas = new Canvas(output);

	    final Paint paint = new Paint();
	    final Rect rect = new Rect(0, 0, finalBitmap.getWidth(), finalBitmap.getHeight());

	    paint.setAntiAlias(true);
	    paint.setFilterBitmap(true);
	    paint.setDither(true);
	    paint.setFlags(Paint.ANTI_ALIAS_FLAG);
	    Paint paintBorder = new Paint();
        setBorderColor(Color.WHITE);
        paintBorder.setAntiAlias(true);
        
        canvas.drawCircle(finalBitmap.getWidth() / 2+0.7f, finalBitmap.getHeight() / 2+0.7f,
	            finalBitmap.getWidth() / 2+0.1f, paint);
	    paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
	    canvas.drawBitmap(finalBitmap, rect, rect, paint);
	            return output;
	}
	private static void setBorderColor(int white) {
		// TODO Auto-generated method stub
		
	}
}

